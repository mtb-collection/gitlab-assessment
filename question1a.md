<!-- #!/bin/bash

# <!-- Write a Ruby or Bash script that will print usernames of all users on a Linux system together with
# their home directories. Here's some example output:

# gitlab:/home/gitlab
# nobody:/nonexistent
# .
# .
# Each line is a concatenation of a username, the colon character (:), and the home directory path
# for that username. Your script should output such a line for each user on the system. --> 



while IFS=':' read -r username _ _ _ homedir _; do
  echo "$username:$homedir"
done < /etc/passwd
