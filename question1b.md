#!/bin/bash


<!-- # Next, write a second script that:
# ● Takes the full output of your first script and converts it to an MD5 hash.
# ● On its first run stores the MD5 checksum into the /var/log/current_users file.
# ● On subsequent runs, if the MD5 checksum changes, the script should add a line in the /var/
# log/user_changes file with the message, DATE TIME changes occurred, replacing DATE
# and TIME with appropriate values, and replaces the old MD5 checksum in /var/log/
# current_users file with the new MD5 checksum. -->

<!-- # Function to calculate MD5 hash -->
calculate_md5() {
  md5sum | awk '{print $1}'
}

<!-- # Check if the log directory exists, if not, create it -->
log_dir="/var/log"
if [ ! -d "$log_dir" ]; then
  mkdir "$log_dir"
fi

<!-- # File paths -->
current_users_file="$log_dir/current_users"
user_changes_file="$log_dir/user_changes"

<!-- # Get the current MD5 hash from the first script -->
md5_output=$(./question1a.sh | md5sum | awk '{print $1}')

<!-- # Check if current_users file exists and read the previous MD5 hash -->
previous_md5=""
if [ -f "$current_users_file" ]
then
  previous_md5=$(cat "$current_users_file")
else
  echo "$md5_output" > "$current_users_file"
  exit 0
fi

<!-- # Compare previous MD5 hash with the current one -->
if [ "$previous_md5" != "$md5_output" ]; then
  <!-- # If MD5 has changed, log the changes -->
  date_time=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date_time changes occurred" >> "$user_changes_file"

  <!-- # Update current_users file with the new MD5 hash -->
  echo "$md5_output" > "$current_users_file"
fi

