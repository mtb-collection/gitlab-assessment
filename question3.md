<!-- Question on commit graph -->
The commit graph shown below shows a feature branch being created and then merged back into the main branch. The sequence of Git commands that could have resulted in this commit graph is as follows:

```
# on the main branch, make file changes then,
git commit -am “first commit”

# make file changes on the main branch, then
git commit -am “second commit”

# create feature-branch from main branch and checkout to the created branch
git checkout -b feature-branch

#make file changes on the feature-branch, then
git commit -am “awesome feature”

#checkout to the main branch
git checkout main

# make file changes on the main branch, then
git commit -am “third commit”

# merge the feature-branch into the main branch
git merge feature-branch

# make files changes on the main branch, then
git commit -am “fourth commit”
```


This sequence of commands would create the following commit graph:
```

git commit -am "first commit": Commits the file changes made on the main branch with the message "first commit."

git commit -am "second commit": Commits the file changes made on the main branch with the message "second commit."

git checkout -b feature-branch: Creates a new branch called "feature-branch" from the current state of the main branch and switches to the new branch.

git commit -am "awesome feature": Commits the file changes made on the "feature-branch" with the message "awesome feature."

git checkout main: Switches back to the main branch.

git commit -am "third commit": Commits the file changes made on the main branch with the message "third commit."

git merge feature-branch: Merges the changes from the "feature-branch" into the main branch. This brings the changes made in the "awesome feature" commit into the main branch.

git commit -am "fourth commit": Commits the file changes made on the main branch with the message "fourth commit."
```



