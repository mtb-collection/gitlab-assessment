## A Beginner’s Guide to Using Git for Implementing New Features without affecting the Main Branch

**Introduction:**

Git is a powerful version control system that allows developers to work collaboratively on projects while keeping track of changes and history. In this tutorial, we will explore how to use Git to implement a new feature or make changes without affecting the main branch of your project. By following these steps, you can safely experiment with new ideas and collaborate with your team without disrupting the stability of the main codebase.

**Prerequisites:**

Before we begin, make sure you have Git installed on your system. You can download and install Git from the official website (https://git-scm.com/downloads). Additionally, you should have a basic understanding of Git concepts, such as branches, commits, and remotes.

**Step 1: Cloning the Repository**

To get started, open your terminal and navigate to the directory where you want to work on the project. Use the following command to clone the repository from the remote server to your local machine:

`git clone <repository_url>`

Replace `<repository_url>` with the URL of the Git repository you want to work on. Cloning creates a local copy of the entire project, including all branches and history.

**Step 2: Creating a New Branch**

Once you have cloned the repository, it’s time to create a new branch where you can work on your feature. A branch is like a separate workspace where you can make changes without affecting the main branch (usually called ‘master’ or ‘main’).

`git checkout -b new-feature`

This command creates a new branch named ‘new-feature’ and switches to it. Now you are ready to work on your changes in isolation from the main branch.

**Step 3: Making Changes and Committing**

Now that you are on the new branch, you can make changes to the code as needed to implement your new feature. Open your favorite code editor, make modifications, and save the files.

To track these changes in Git, you need to commit them. A commit is a snapshot of the changes you’ve made. Use the following command to create a commit:

```
git add .   # This adds all the changes to the staging area
git commit -m "Implement new feature: description"
```


The first command (`git add .`) stages all the changes you made, and the second command (git commit) creates a commit with a descriptive message explaining the changes you made. Be clear and concise in your commit messages to make it easier for others to understand your changes later.

**Step 4: Pushing the Changes to the Remote Repository**

After committing your changes, it’s time to push them to the remote repository. This will make your changes visible to other team members and allow you to collaborate seamlessly.

`git push origin new-feature`

The above command pushes the changes you made on the ‘new-feature’ branch to the remote repository.

**Step 5: Creating a Merge Request (MR)**

In most Git workflows, you create a Merge Request (MR) to merge your changes into the main branch. This allows your teammates to review your code and discuss the changes before they are merged.

To create a MR, go to the repository’s web page (GitLab), select the `‘new-feature’` branch, and click on the “Create Merge Request” button. Add a meaningful title and description, and then submit the MR.

**Step 6: Review and Merge**

Once your MR is submitted, your team will review the changes. If everything looks good, a team member with the appropriate permissions will merge your changes into the main branch.

**Conclusion:**

Using Git to implement new features without affecting the main branch is a fundamental skill for collaborating effectively with others. By following these steps, you can confidently contribute to projects, experiment with new ideas, and maintain a clean and organized codebase.

Remember always to communicate with your team, use descriptive commit messages, and leverage the power of branches to keep your workflow efficient and organised. Happy coding!
